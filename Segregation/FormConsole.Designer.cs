﻿namespace Segregation
{
    partial class FormConsole
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.GroupBox groupBox2;
            System.Windows.Forms.Label label3;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormConsole));
            this.pgsBar_time = new System.Windows.Forms.ProgressBar();
            this.txtBox_time = new System.Windows.Forms.TextBox();
            this.label_progress = new System.Windows.Forms.Label();
            this.txtBox_lastTime = new System.Windows.Forms.TextBox();
            this.txtBox_output = new System.Windows.Forms.TextBox();
            this.timer_update = new System.Windows.Forms.Timer(this.components);
            this.btn_break = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txt_maxIter = new System.Windows.Forms.TextBox();
            this.txt_iter = new System.Windows.Forms.TextBox();
            this.check_outputPause = new System.Windows.Forms.CheckBox();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            groupBox2 = new System.Windows.Forms.GroupBox();
            label3 = new System.Windows.Forms.Label();
            groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(12, 26);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(108, 13);
            label1.TabIndex = 3;
            label1.Text = "Время выполнения:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(12, 122);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(112, 13);
            label2.TabIndex = 7;
            label2.Text = "Примерно осталось:";
            // 
            // groupBox2
            // 
            groupBox2.Controls.Add(label1);
            groupBox2.Controls.Add(this.pgsBar_time);
            groupBox2.Controls.Add(this.txtBox_time);
            groupBox2.Controls.Add(this.label_progress);
            groupBox2.Controls.Add(this.txtBox_lastTime);
            groupBox2.Controls.Add(label2);
            groupBox2.Location = new System.Drawing.Point(374, 12);
            groupBox2.Name = "groupBox2";
            groupBox2.Size = new System.Drawing.Size(202, 158);
            groupBox2.TabIndex = 11;
            groupBox2.TabStop = false;
            groupBox2.Text = "Прогресс";
            // 
            // pgsBar_time
            // 
            this.pgsBar_time.Location = new System.Drawing.Point(15, 60);
            this.pgsBar_time.Name = "pgsBar_time";
            this.pgsBar_time.Size = new System.Drawing.Size(133, 23);
            this.pgsBar_time.TabIndex = 2;
            // 
            // txtBox_time
            // 
            this.txtBox_time.BackColor = System.Drawing.SystemColors.Control;
            this.txtBox_time.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBox_time.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtBox_time.ForeColor = System.Drawing.Color.Navy;
            this.txtBox_time.Location = new System.Drawing.Point(130, 24);
            this.txtBox_time.Name = "txtBox_time";
            this.txtBox_time.ReadOnly = true;
            this.txtBox_time.Size = new System.Drawing.Size(60, 20);
            this.txtBox_time.TabIndex = 5;
            this.txtBox_time.TabStop = false;
            this.txtBox_time.Text = "00h 00m";
            this.txtBox_time.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label_progress
            // 
            this.label_progress.AutoSize = true;
            this.label_progress.Location = new System.Drawing.Point(157, 64);
            this.label_progress.Name = "label_progress";
            this.label_progress.Size = new System.Drawing.Size(21, 13);
            this.label_progress.TabIndex = 8;
            this.label_progress.Text = "0%";
            this.label_progress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtBox_lastTime
            // 
            this.txtBox_lastTime.BackColor = System.Drawing.SystemColors.Control;
            this.txtBox_lastTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBox_lastTime.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtBox_lastTime.ForeColor = System.Drawing.Color.Navy;
            this.txtBox_lastTime.Location = new System.Drawing.Point(130, 120);
            this.txtBox_lastTime.Name = "txtBox_lastTime";
            this.txtBox_lastTime.ReadOnly = true;
            this.txtBox_lastTime.Size = new System.Drawing.Size(60, 20);
            this.txtBox_lastTime.TabIndex = 6;
            this.txtBox_lastTime.TabStop = false;
            this.txtBox_lastTime.Text = "00h 00m";
            this.txtBox_lastTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(76, 5);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(19, 13);
            label3.TabIndex = 9;
            label3.Text = "из";
            // 
            // txtBox_output
            // 
            this.txtBox_output.BackColor = System.Drawing.Color.White;
            this.txtBox_output.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBox_output.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtBox_output.Location = new System.Drawing.Point(12, 12);
            this.txtBox_output.MaxLength = 0;
            this.txtBox_output.Multiline = true;
            this.txtBox_output.Name = "txtBox_output";
            this.txtBox_output.ReadOnly = true;
            this.txtBox_output.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtBox_output.Size = new System.Drawing.Size(356, 318);
            this.txtBox_output.TabIndex = 0;
            // 
            // timer_update
            // 
            this.timer_update.Tick += new System.EventHandler(this.timer_update_Tick);
            // 
            // btn_break
            // 
            this.btn_break.BackColor = System.Drawing.Color.Red;
            this.btn_break.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_break.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btn_break.Location = new System.Drawing.Point(501, 307);
            this.btn_break.Name = "btn_break";
            this.btn_break.Size = new System.Drawing.Size(75, 23);
            this.btn_break.TabIndex = 9;
            this.btn_break.Text = "Прервать";
            this.btn_break.UseVisualStyleBackColor = false;
            this.btn_break.Visible = false;
            this.btn_break.Click += new System.EventHandler(this.btn_break_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Location = new System.Drawing.Point(374, 174);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(202, 78);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Перестановки атомов";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.txt_maxIter);
            this.panel1.Controls.Add(this.txt_iter);
            this.panel1.Controls.Add(label3);
            this.panel1.Location = new System.Drawing.Point(14, 31);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(172, 26);
            this.panel1.TabIndex = 11;
            // 
            // txt_maxIter
            // 
            this.txt_maxIter.BackColor = System.Drawing.SystemColors.Control;
            this.txt_maxIter.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_maxIter.Cursor = System.Windows.Forms.Cursors.Default;
            this.txt_maxIter.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txt_maxIter.Location = new System.Drawing.Point(101, 5);
            this.txt_maxIter.Name = "txt_maxIter";
            this.txt_maxIter.ReadOnly = true;
            this.txt_maxIter.Size = new System.Drawing.Size(60, 13);
            this.txt_maxIter.TabIndex = 10;
            this.txt_maxIter.TabStop = false;
            this.txt_maxIter.Text = "24000";
            this.txt_maxIter.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_iter
            // 
            this.txt_iter.BackColor = System.Drawing.SystemColors.Control;
            this.txt_iter.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_iter.Cursor = System.Windows.Forms.Cursors.Default;
            this.txt_iter.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txt_iter.Location = new System.Drawing.Point(10, 5);
            this.txt_iter.Name = "txt_iter";
            this.txt_iter.ReadOnly = true;
            this.txt_iter.Size = new System.Drawing.Size(60, 13);
            this.txt_iter.TabIndex = 7;
            this.txt_iter.TabStop = false;
            this.txt_iter.Text = "5432";
            this.txt_iter.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // check_outputPause
            // 
            this.check_outputPause.AutoSize = true;
            this.check_outputPause.Cursor = System.Windows.Forms.Cursors.Help;
            this.check_outputPause.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.check_outputPause.Location = new System.Drawing.Point(374, 258);
            this.check_outputPause.Name = "check_outputPause";
            this.check_outputPause.Size = new System.Drawing.Size(136, 17);
            this.check_outputPause.TabIndex = 12;
            this.check_outputPause.Text = "Приостановить вывод";
            this.toolTip.SetToolTip(this.check_outputPause, "Приостанавливает вывод информации в текстовое окно\r\n(при этом вычисления продолжа" +
        "ются), после снятия флажка\r\nв окно выводится вся информация до текущего момента " +
        "выполнения");
            this.check_outputPause.UseVisualStyleBackColor = true;
            // 
            // toolTip
            // 
            this.toolTip.AutoPopDelay = 8000;
            this.toolTip.InitialDelay = 500;
            this.toolTip.ReshowDelay = 100;
            // 
            // FormConsole
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(588, 342);
            this.Controls.Add(this.check_outputPause);
            this.Controls.Add(groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_break);
            this.Controls.Add(this.txtBox_output);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormConsole";
            this.Text = "Информация о моделировании";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormConsole_FormClosing);
            this.Load += new System.EventHandler(this.FormConsole_Load);
            groupBox2.ResumeLayout(false);
            groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtBox_output;
        private System.Windows.Forms.ProgressBar pgsBar_time;
        private System.Windows.Forms.Timer timer_update;
        private System.Windows.Forms.TextBox txtBox_time;
        private System.Windows.Forms.TextBox txtBox_lastTime;
        private System.Windows.Forms.Label label_progress;
        private System.Windows.Forms.Button btn_break;
        private System.Windows.Forms.TextBox txt_maxIter;
        private System.Windows.Forms.TextBox txt_iter;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox check_outputPause;
        private System.Windows.Forms.ToolTip toolTip;
    }
}