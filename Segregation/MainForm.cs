﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using LibDrawingGraphs;

namespace Segregation
{
    public partial class MainForm : Form
    {
        //Объект класса сегрегации
        Segregation sgr;
        //Массивы энергий
        double[] kinEnergy, potEnergy, oPotEnergy, sumEnergy, masTimeEnergy;
        double[][] listMasEnergy;
        int idxMasEnergy = 0;
        int listSize = 0;
        //Объекты графических классов
        DrawingGraphs graphPot, graphSwap;
        DrawingGraphs graphKin, graphSum;

        Random RandNumGen, RandCreate, RandShift;
        int seedNumGen;
        private FormOptions.ModelingParams modelParams;


        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            OutputInfo.Start();

            graphPot = new DrawingGraphs(pict_potEnergy.Width, pict_potEnergy.Height);
            graphKin = new DrawingGraphs(pict_kinEnergy.Width, pict_kinEnergy.Height);
            graphSum = new DrawingGraphs(pict_3.Width, pict_3.Height);
            graphSwap = new DrawingGraphs(pict_swap.Width, pict_swap.Height);

            int testNum = OutputInfo.ReadTestNum(true);
            if (testNum == -1) label_testNum.Text = "Проведено экспериментов: -";
            else label_testNum.Text = "Проведено экспериментов: " + testNum.ToString();

            modelParams = new FormOptions.ModelingParams(0, 0, true);

            // затравка в виде [день|час|минуты|милисекунды]
            seedNumGen = Convert.ToInt32(
                (DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() +
                DateTime.Now.Millisecond.ToString()));

            RandNumGen = new Random(seedNumGen);
            RandCreate = RandShift = RandNumGen;

            grpBox_energy.Enabled = false;
            grpBox_graph.Enabled = false;
            grpBox_relax.Enabled = false;
            grpBox_swap.Enabled = false;

            label_status.Text = string.Empty;
        }

        /// <summary>
        /// Скриншот окна приложения
        /// </summary>
        /// <param name="testNum">Номер теста</param>
        private void Screen(int testNum)
        {
            Rectangle bounds = this.Bounds;
            using (Bitmap bitmap = new Bitmap(bounds.Width, bounds.Height))
            {
                using (Graphics g = Graphics.FromImage(bitmap))
                {
                    g.CopyFromScreen(new Point(bounds.Left, bounds.Top), Point.Empty, bounds.Size);
                }
                bitmap.Save(Environment.CurrentDirectory + "\\Data\\Tests\\" + String.Format("screen_{0}_{1}.jpg", DateTime.Today.Date.ToString("yyMMdd"), testNum));
            }
        }

        private void SetStatusLabel(bool ready)
        {
            if (ready)
            {
                label_status.Text = "Ожидание запуска";
                label_status.ForeColor = Color.Red;

            }
            else
            {
                label_status.Text = "Завершено";
                label_status.ForeColor = Color.Green;
            }
        }

        //Метод для отрисовки графиков
        private void DrawGraph(int numStep, bool bTime)
        {
            string textAxisX;

            if (bTime)
            {
                textAxisX = "Шаг по времени";

                graphPot.AddGraph(potEnergy, masTimeEnergy, Color.DarkBlue);
                graphPot.DrawGraphs(textAxisX, "Энергия (эВ)", "График потенциальной энергии");
                pict_potEnergy.Image = graphPot.GetImage;

                double energy = Convert.ToDouble(txt_tmprEnergy.Text);
                graphKin.AddAxisX = true;
                graphKin.AddGraph(new double[] { energy, energy }, 0, masTimeEnergy.Max(), Color.Black);
                graphKin.AddGraph(kinEnergy, masTimeEnergy, Color.Red);
                graphKin.DrawGraphs(textAxisX, "Энергия (эВ)", "График кинетической энергии");
                pict_kinEnergy.Image = graphKin.GetImage;

                graphSum.AddAxisX = true;
                graphSum.AddGraph(sumEnergy, masTimeEnergy, Color.DarkGreen);
                graphSum.AddGraph(kinEnergy, masTimeEnergy, Color.Red);
                graphSum.AddGraph(oPotEnergy, masTimeEnergy, Color.DarkBlue);
                graphSum.DrawGraphs(textAxisX, "Энергия (эВ)", "График энергий");
                pict_3.Image = graphSum.GetImage;
            }
            else
            {
                textAxisX = "Номер шага по времени";

                graphPot.IntValueX = true;
                graphPot.AddGraph(potEnergy, 0, numStep, Color.DarkBlue);
                graphPot.DrawGraphs(textAxisX, "Энергия (эВ)", "График потенциальной энергии");
                pict_potEnergy.Image = graphPot.GetImage;
                graphPot.IntValueX = false;

                double energy = Convert.ToDouble(txt_tmprEnergy.Text);
                graphKin.IntValueX = true;
                graphKin.AddAxisX = true;
                graphKin.AddGraph(new double[] { energy, energy }, 0, numStep, Color.Black);
                graphKin.AddGraph(kinEnergy, 0, numStep, Color.Red);
                graphKin.DrawGraphs(textAxisX, "Энергия (эВ)", "График кинетической энергии");
                pict_kinEnergy.Image = graphKin.GetImage;
                graphKin.IntValueX = false;

                graphSum.IntValueX = true;
                graphSum.AddAxisX = true;
                graphSum.AddGraph(sumEnergy, 0, numStep, Color.DarkGreen);
                graphSum.AddGraph(kinEnergy, 0, numStep, Color.Red);
                graphSum.AddGraph(oPotEnergy, 0, numStep, Color.DarkBlue);
                graphSum.DrawGraphs(textAxisX, "Энергия (эВ)", "График энергий");
                pict_3.Image = graphSum.GetImage;
                graphSum.IntValueX = false;
            }
        }
        //Метод для отрисовки пустых графиков с сеткой
        private void DrawStartGraph(bool bTime)
        {
            string textAxisX;
            if (bTime)
                textAxisX = "Шаг по времени";
            else textAxisX = "Номер шага по времени";


            graphPot.DrawGraphs(textAxisX, "Энергия (эВ)", "График потенциальной энергии", true);
            pict_potEnergy.Image = graphPot.GetImage;

            graphKin.DrawGraphs(textAxisX, "Энергия (эВ)", "График кинетической энергии", true);
            pict_kinEnergy.Image = graphKin.GetImage;

            graphSum.DrawGraphs(textAxisX, "Энергия (эВ)", "График энергий", true);
            pict_3.Image = graphSum.GetImage;

            //graphSwap.DrawGraphs(textAxisX, "Энергия (эВ)", "График потенциальной энергии", true);
            //pict_swap.Image = graphSwap.GetImage;

        }

        //Создание структуры
        private void btn_createStructure_Click(object sender, EventArgs e)
        {
            if (modelParams.seedCreation != 0)
                RandCreate = new Random(modelParams.seedCreation);
            else RandCreate = RandNumGen;

            FormLoad.MessageHandler handler = delegate
            {
                sgr = new Segregation((int)numUD_sizeStructure.Value, (double)numUD_partGe.Value, RandCreate);

                //выбор потенциала и подсчёт энергии
                rdBtn_TRS_CheckedChanged(null, null);
                //Вычисление равновесной энергии
                numUD_tmpr_ValueChanged(null, null);
                //расчёт времени релаксации
                numUD_numStepRelax_ValueChanged(null, null);

                SetStatusLabel(true);
                if (!grpBox_energy.Enabled)
                {
                    grpBox_energy.Enabled = true;
                    grpBox_graph.Enabled = true;
                    grpBox_relax.Enabled = true;
                    grpBox_swap.Enabled = true;
                    check_corrFunc.Checked = true;

                    DrawStartGraph(checkBox_timeRelax.Checked);
                    graphSwap.DrawGraphs("Номер перестановки", "Энергия (эВ)", "График потенциальной энергии", true);
                    pict_swap.Image = graphSwap.GetImage;
                }

            };

            FormLoad formLoad = new FormLoad("Сборка структуры", handler);
            formLoad.ShowDialog();
        }

        //Метод переключения вывода полной энергии, либо изменения энергии
        private void radioBtn_graphFull_CheckedChanged(object sender, EventArgs e)
        {
            if (kinEnergy == null || kinEnergy.Length < 3)
            {
                DrawStartGraph(checkBox_timeRelax.Checked);
                return;
            }

            //int numStep = (int)numUD_numStepRelax.Value;
            DrawGraph(kinEnergy.Length - 1, checkBox_timeRelax.Checked);
        }

        //Нажатие на кнопку выход
        private void escapeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        //Метод подсчёта энергии структуры
        private void btn_calculateEnergy_Click(object sender, EventArgs e)
        {
            FormLoad.MessageHandler handler = delegate
            {
                double energy = sgr.PotentialEnergy();
                txt_sumEnergy.Text = energy.ToString("f6");
                txt_atomEnergy.Text = (energy / sgr.StructNumAtoms).ToString("f6");
            };

            if (sender != null)
            {
                FormLoad formLoad = new FormLoad("Вычисление энергии", handler);
                formLoad.ShowDialog();
            }
            else handler();
        }

        //Метод релаксации структуры
        private void btn_relaxation_Click(object sender, EventArgs e)
        {
            //Определяем параметры
            int numStep = (int)numUD_numStepRelax.Value;
            double temperature = (double)numUD_tmpr.Value;
            double shift = (double)numUD_shiftAtoms.Value;
            bool normalize = checkBox_startVelocity.Checked;
            int stepNorm = (int)numUD_stepNorm.Value;
            bool justRelax = checkBox_justRelax.Checked;
            bool calcCorrFunc = check_corrFunc.Checked;

            //Формируем шаги по времени
            List<int> stepDev = new List<int>(listBox_step.Items.Count);
            for (int i = 0; i < listBox_step.Items.Count; i++)
                stepDev.Add((int)listBox_step.Items[i]);

            int countSwap = (int)numUD_swap.Value;
            int periodSwap = (int)numUD_periodSwap.Value;
            int periodIdent = (int)numUD_ident.Value;

            if (modelParams.seedShift != 0)
                RandShift = new Random(modelParams.seedShift);
            else RandShift = RandNumGen;

            Relaxation relax = new Relaxation(sgr);

            FormLoad.MessageHandler handler = delegate
            {
                relax.Initialization(RandNumGen, RandShift, temperature, shift, normalize,
                    stepDev.ToArray(), (double)numUD_multStepRelax.Value, numStep, stepNorm, justRelax, countSwap, periodSwap, periodIdent, calcCorrFunc);

                relax.CalculatedTime();
            };

            FormLoad formLoad = new FormLoad("Подготовка данных", handler);
            formLoad.ShowDialog();

            if (MessageBox.Show(String.Format("Примерное время процесса - {0}h {1}m\nПродолжить?", relax.ProcTime.Hours, relax.ProcTime.Minutes),
                "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;

            //Проверяем и очищаем файлы Excel перед релаксацией
            bool success = OutputInfo.ClearEnergyFile(false);
            if (!success)
                if (MessageBox.Show("Обнаружено открытие используемого файла Excel - данные не будут записаны. Продолжить выполнение?",
                    "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;
            if (periodSwap != 0)
            {
                success = OutputInfo.ClearDistrFile(false);
                if (!success)
                    if (MessageBox.Show("Обнаружено открытие используемого файла Excel - данные не будут записаны. Продолжить выполнение?",
                        "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;

            }

            //Ряд тестов
            int testNum = OutputInfo.ReadTestNum(false);
            if (testNum == -1)
            {
                MessageBox.Show("Открыт конфигурационый файл.\n Для продолжения работы программы закройте его.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            OutputInfo.ClearEnergyFile(true, temperature);
            OutputInfo.ClearDistrFile(true, temperature);

            //Корреляционная функция
            if (calcCorrFunc)
            {
                success = OutputInfo.ClearCorrFunction(temperature);
                if (!success)
                    if (MessageBox.Show("Обнаружено открытие используемого файла Excel - данные не будут записаны. Продолжить выполнение?",
                        "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;
            }

            // Релаксация

            if (modelParams.minMainForm)
            {
                this.WindowState = FormWindowState.Minimized;
                this.ShowInTaskbar = false;
            }

            //Проводим релаксацию
            Task task = Task.Factory.StartNew(delegate { relax.Iterations(); }, TaskCreationOptions.LongRunning);

            FormConsole formConsole = new FormConsole(relax);
            formConsole.ShowDialog();

            if (modelParams.minMainForm)
            {
                this.ShowInTaskbar = true;
                this.WindowState = FormWindowState.Normal;
            }



            handler = delegate
            {
                btn_calculateEnergy_Click(null, null);
                SetStatusLabel(false);
                label_testNum.Text = "Проведено экспериментов: " + testNum.ToString();

                //kinEnergy, potEnergy, oPotEnergy, sumEnergy, masTimeEnergy;
                kinEnergy = new double[relax.GetMasTimeStep.Length];
                potEnergy = new double[relax.GetMasTimeStep.Length];
                masTimeEnergy = new double[relax.GetMasTimeStep.Length];

                //Получаем энергию
                sumEnergy = new double[kinEnergy.Length];
                oPotEnergy = new double[kinEnergy.Length];
                for (int i = 0; i < kinEnergy.Length; i++)
                {
                    kinEnergy[i] = relax.GetMasKinEnergy[i];
                    potEnergy[i] = relax.GetMasPotEnergy[i];
                    masTimeEnergy[i] = relax.GetMasTimeStep[i];

                    oPotEnergy[i] = potEnergy[i] - potEnergy[0];
                    sumEnergy[i] = oPotEnergy[i] + kinEnergy[i];
                }

                //Отрисовываем
                if (kinEnergy.Length >= 3) DrawGraph(kinEnergy.Length - 1, checkBox_timeRelax.Checked);

                //перестановки
                listMasEnergy = relax.GetAlgSwap.GetListMasEnergy();
                if (listMasEnergy == null) listSize = 0;
                else listSize = listMasEnergy.Count();

                if (listSize > 0)
                {
                    if (numUD_graphsSwap.Maximum == listSize)
                    {
                        if (numUD_graphsSwap.Value == numUD_graphsSwap.Maximum) DrawGraphSwap();
                        else numUD_graphsSwap.Value = numUD_graphsSwap.Maximum;
                    }
                    else
                    {
                        numUD_graphsSwap.Maximum = listSize;
                        numUD_graphsSwap.Value = numUD_graphsSwap.Maximum;
                        numUD_graphsSwap.Minimum = 1;
                    }
                }
                else
                {
                    numUD_graphsSwap.Maximum = numUD_graphsSwap.Minimum = 0;
                    graphSwap.DrawGraphs("Номер перестановки", "Энергия (эВ)", "График потенциальной энергии", true);
                    pict_swap.Image = graphSwap.GetImage;
                }

                txt_graphsSwap.Text = String.Format("{0}", listSize);
            };
            formLoad = new FormLoad("Сохранение данных", handler);
            formLoad.ShowDialog();

            this.Activate();
            timer_screen.Start();
        }

        //Открыть папку данных
        private void dataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OutputInfo.OpenStorage();
        }

        //Открыть в Excel
        private void btn_openGraph_Click(object sender, EventArgs e)
        {
            if (!OutputInfo.OpenGraphEnergy()) MessageBox.Show("Данного файла не существует", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        //Методы удаления шага, очищения списка шагов, добавление шага
        private void btn_deleteStep_Click(object sender, EventArgs e)
        {
            int indx = listBox_step.SelectedIndex;
            if (indx > -1)
            {
                listBox_step.Items.RemoveAt(indx);

                //перерасчёт времени релаксации
                numUD_numStepRelax_ValueChanged(null, null);
            }
        }
        private void btn_clearStep_Click(object sender, EventArgs e)
        {
            if (listBox_step.Items.Count > 0)
            {
                listBox_step.Items.Clear();

                //перерасчёт времени релаксации
                numUD_numStepRelax_ValueChanged(null, null);
            }
        }

        private void btn_addStep_Click(object sender, EventArgs e)
        {
            int step = (int)numUD_step.Value;
            if (!listBox_step.Items.Contains(step))
            {
                listBox_step.Items.Add(step);

                if (listBox_step.Items.Count > 1)
                {
                    List<int> list = new List<int>(listBox_step.Items.Count);
                    for (int i = 0; i < listBox_step.Items.Count; i++)
                        list.Add((int)listBox_step.Items[i]);
                    list.Sort();
                    for (int i = 0; i < listBox_step.Items.Count; i++)
                        listBox_step.Items[i] = list[i];
                    list.Clear();
                }

                //перерасчёт времени релаксации
                numUD_numStepRelax_ValueChanged(null, null);
            }
        }

        //Метод закрытия формы
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Хотите выйти из приложения?", "Выход", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
                e.Cancel = true;
        }

        private void rdBtn_TRS_CheckedChanged(object sender, EventArgs e)
        {
            sgr.SetPotential(rdBtn_TRS.Checked);
            //Подсчёт энергии
            if (sender != null) btn_calculateEnergy_Click(sender, null);
            else btn_calculateEnergy_Click(null, null);
        }

        private void DrawGraphSwap()
        {
            if (listMasEnergy == null) return;

            if (listMasEnergy[idxMasEnergy].Count() >= 2)
            {
                graphSwap.IntValueX = true;
                graphSwap.AddGraph(listMasEnergy[idxMasEnergy], 0, listMasEnergy[idxMasEnergy].Count(), Color.DarkBlue);
                graphSwap.DrawGraphs("Номер перестановки", "Энергия (эВ)", "График потенциальной энергии");
                pict_swap.Image = graphSwap.GetImage;
                graphSwap.IntValueX = false;
            }
            else
            {
                graphSwap.DrawGraphs("Номер перестановки", "Энергия (эВ)", "График потенциальной энергии", true);
                pict_swap.Image = graphSwap.GetImage;
            }

            //txt_graphsSwap.Text = String.Format("{0}", listSize);
        }

        private void numUD_graphsSwap_ValueChanged(object sender, EventArgs e)
        {
            idxMasEnergy = (int)numUD_graphsSwap.Value - 1;

            if (listSize > 0)
            {
                DrawGraphSwap();
            }
        }

        private void numUD_numStepRelax_ValueChanged(object sender, EventArgs e)
        {
            double time = 0;
            int last = 0;
            double mod = (double)numUD_multStepRelax.Value;

            for (int i = 0; i < listBox_step.Items.Count; i++)
            {
                int step = (int)listBox_step.Items[i] - last;
                last = (int)listBox_step.Items[i];

                time += step * mod;
                mod /= 2.0;
            }
            time += ((int)numUD_numStepRelax.Value - last) * mod;
            txt_timetau.Text = String.Format("({0:f1}τ)", time);

            time *= AtomParams.PERIOD_OSCILL * 1e+12;
            txt_time.Text = String.Format("{0:f3}", time);

        }

        private void checkBox_startVelocity_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_startVelocity.Checked) numUD_shiftAtoms.Value = 0;
        }

        private void numUD_shiftAtoms_ValueChanged(object sender, EventArgs e)
        {
            if (numUD_shiftAtoms.Value != 0) checkBox_startVelocity.Checked = false;
        }

        private void label_swap_MouseEnter(object sender, EventArgs e)
        {
            numUD_swap.BackColor = Color.Red;
            numUD_swap.ForeColor = Color.White;
        }

        private void label_swap_MouseLeave(object sender, EventArgs e)
        {
            if (numUD_swap.BackColor == Color.Red)
            {
                numUD_swap.BackColor = Color.FromName("Window");
                numUD_swap.ForeColor = Color.FromName("WindowText");
            }
        }

        private void btn_openNeigh_Click(object sender, EventArgs e)
        {
            if (!OutputInfo.OpenDistr()) MessageBox.Show("Данного файла не существует", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void timer_screen_Tick(object sender, EventArgs e)
        {
            Screen(OutputInfo.GetTestNum);
            timer_screen.Stop();
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormOptions formOptions = new FormOptions(modelParams);
            formOptions.ShowDialog();

            modelParams = formOptions.GetModelParams;
        }

        //Вычисление параметра решётки
        private void numUD_partGe_ValueChanged(object sender, EventArgs e)
        {
            txt_latPar.Text = Segregation.AtomLatticeParam((double)numUD_partGe.Value).ToString();
        }

        //Метод подсчёта количества атомов структуры в зависимости от её размера
        private void numUD_sizeStructure_ValueChanged(object sender, EventArgs e)
        {
            int sizeStruct = (int)numUD_sizeStructure.Value;
            int numAtoms = Segregation.TotalNumAtoms(sizeStruct);
            txt_numAtoms.Text = numAtoms.ToString();

            double value = Math.Round(numAtoms * 3.0 / 100) * 100;
            numUD_swap.Value = (int)value;
        }

        //Метод подсчёта равновесной температуры для заданной структуры
        private void numUD_tmpr_ValueChanged(object sender, EventArgs e)
        {
            double tmpr = (double)numUD_tmpr.Value;
            double energy = Segregation.TemperatureEnergy(tmpr, sgr.StructNumAtoms);
            txt_tmprEnergy.Text = energy.ToString("f6");
        }

        //Метод вывода окна формул, по которым проводились расчёты
        private void formulasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormFormulas formFormulas = new FormFormulas();
            formFormulas.Show();
        }

        //Вывод информации о программе
        private void infoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormAbout formAbout = new FormAbout();
            formAbout.ShowDialog();
        }
    }
}