﻿namespace Segregation
{
    partial class FormOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormOptions));
            this.btn_close = new System.Windows.Forms.Button();
            this.numUD_randCreate = new System.Windows.Forms.NumericUpDown();
            this.numUD_randShift = new System.Windows.Forms.NumericUpDown();
            this.check_minMainForm = new System.Windows.Forms.CheckBox();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_randCreate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_randShift)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Cursor = System.Windows.Forms.Cursors.Help;
            label1.Location = new System.Drawing.Point(12, 19);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(195, 13);
            label1.TabIndex = 4;
            label1.Text = "Затравка для генератора структуры:";
            this.toolTip.SetToolTip(label1, "Если записан 0, то в качестве затравки используется местное время");
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Cursor = System.Windows.Forms.Cursors.Help;
            label2.Location = new System.Drawing.Point(12, 45);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(218, 13);
            label2.TabIndex = 4;
            label2.Text = "Затравка для генератора сдвига атомов:";
            this.toolTip.SetToolTip(label2, "Если записан 0, то в качестве затравки используется местное время");
            // 
            // btn_close
            // 
            this.btn_close.BackColor = System.Drawing.Color.White;
            this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_close.Location = new System.Drawing.Point(279, 159);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(75, 23);
            this.btn_close.TabIndex = 0;
            this.btn_close.Text = "Закрыть";
            this.btn_close.UseVisualStyleBackColor = false;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // numUD_randCreate
            // 
            this.numUD_randCreate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numUD_randCreate.Location = new System.Drawing.Point(213, 17);
            this.numUD_randCreate.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numUD_randCreate.Name = "numUD_randCreate";
            this.numUD_randCreate.Size = new System.Drawing.Size(80, 20);
            this.numUD_randCreate.TabIndex = 1;
            // 
            // numUD_randShift
            // 
            this.numUD_randShift.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numUD_randShift.Location = new System.Drawing.Point(236, 43);
            this.numUD_randShift.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numUD_randShift.Name = "numUD_randShift";
            this.numUD_randShift.Size = new System.Drawing.Size(80, 20);
            this.numUD_randShift.TabIndex = 2;
            // 
            // check_minMainForm
            // 
            this.check_minMainForm.AutoSize = true;
            this.check_minMainForm.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.check_minMainForm.Checked = true;
            this.check_minMainForm.CheckState = System.Windows.Forms.CheckState.Checked;
            this.check_minMainForm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.check_minMainForm.Location = new System.Drawing.Point(12, 78);
            this.check_minMainForm.Name = "check_minMainForm";
            this.check_minMainForm.Size = new System.Drawing.Size(319, 17);
            this.check_minMainForm.TabIndex = 3;
            this.check_minMainForm.Text = "Сворачивать главное окно после запуска моделирования";
            this.check_minMainForm.UseVisualStyleBackColor = true;
            // 
            // FormOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(366, 194);
            this.Controls.Add(label2);
            this.Controls.Add(label1);
            this.Controls.Add(this.check_minMainForm);
            this.Controls.Add(this.numUD_randShift);
            this.Controls.Add(this.numUD_randCreate);
            this.Controls.Add(this.btn_close);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormOptions";
            this.Text = "Настройка параметров моделирования";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormOptions_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.numUD_randCreate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_randShift)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.NumericUpDown numUD_randCreate;
        private System.Windows.Forms.NumericUpDown numUD_randShift;
        private System.Windows.Forms.CheckBox check_minMainForm;
        private System.Windows.Forms.ToolTip toolTip;
    }
}